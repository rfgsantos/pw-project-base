import {Component, OnInit} from '@angular/core';
import {DetailModel} from '../../shared/models/detail.model';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public details: Array<DetailModel> = new Array<DetailModel>();

  constructor() {
  }

  public ngOnInit(): void {
    this.initDetailsArray();
  }

  private initDetailsArray(): void {
    const firstDetail: DetailModel = new DetailModel();
    firstDetail.headerText = 'Header 1';
    firstDetail.contentText = `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Aenean vel placerat arcu. Donec mattis quam est, eget vehicula dolor
      pellentesque sed. Mauris fringilla blandit mauris cursus malesuada. Donec quis bibendum ex. Phasellus nec elit nec massa luctus
      blandit. Donec vestibulum nisi in condimentum gravida. Curabitur vitae nisi eu ipsum iaculis facilisis. Duis aliquet felis in ante
      placerat, at elementum massa faucibus. Pellentesque scelerisque nisl nulla, sed scelerisque nulla ornare sit amet.`;
    firstDetail.buttonText = 'Button 1';
    firstDetail.icon = 'bi-chevron-bar-right';

    const secondDetail: DetailModel = new DetailModel();
    secondDetail.headerText = 'Header 2';
    secondDetail.contentText = `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Aenean vel placerat arcu. Donec mattis quam est, eget vehicula dolor
      pellentesque sed. Mauris fringilla blandit mauris cursus malesuada. Donec quis bibendum ex. Phasellus nec elit nec massa luctus
      blandit. Donec vestibulum nisi in condimentum gravida. Curabitur vitae nisi eu ipsum iaculis facilisis. Duis aliquet felis in ante
      placerat, at elementum massa faucibus. Pellentesque scelerisque nisl nulla, sed scelerisque nulla ornare sit amet.`;
    secondDetail.buttonText = 'Button 2';
    secondDetail.icon = 'bi-chevron-compact-right';

    const thirdDetail: DetailModel = new DetailModel();
    thirdDetail.headerText = 'Header 3';
    thirdDetail.contentText = `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Aenean vel placerat arcu. Donec mattis quam est, eget vehicula dolor
      pellentesque sed. Mauris fringilla blandit mauris cursus malesuada. Donec quis bibendum ex. Phasellus nec elit nec massa luctus
      blandit. Donec vestibulum nisi in condimentum gravida. Curabitur vitae nisi eu ipsum iaculis facilisis. Duis aliquet felis in ante
      placerat, at elementum massa faucibus. Pellentesque scelerisque nisl nulla, sed scelerisque nulla ornare sit amet.`;
    thirdDetail.buttonText = 'Button 3';
    thirdDetail.icon = 'bi-caret-right-fill';

    this.details.push(firstDetail);
    this.details.push(secondDetail);
    this.details.push(thirdDetail);

  }

}
