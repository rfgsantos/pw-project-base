import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgbButtonsModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NavbarComponent} from './layout/navbar/navbar.component';
import {MainComponent} from './layout/main/main.component';
import {FooterComponent} from './layout/footer/footer.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from './shared/shared.module';
import {UpdateProjectComponent} from './project/update-project/update-project.component';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireStorageModule} from '@angular/fire/storage';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {ListProjectsComponent} from './project/list-projects/list-projects.component';
import {ProjectDetailsComponent} from './project/project-details/project-details.component';
import {DeleteProjectModalComponent} from './project/delete-project-modal.component';
import {OtherProjectsComponent} from './project/other-projects.component';
import {PersonalProjectsComponent} from './project/personal-projects.component';
import {ListAcademicStudiesComponent} from './education/academic-studies/list-academic-studies/list-academic-studies.component';
import {ListCertificationsComponent} from './education/certifications/list-certifications/list-certifications.component';
import {RECAPTCHA_SETTINGS, RecaptchaFormsModule, RecaptchaModule, RecaptchaSettings} from 'ng-recaptcha';

const firebaseConfig = {
  apiKey: 'AIzaSyAu04XbVdsOMxuVmfdj_373YCPi_pehZAI',
  authDomain: 'my-linked-in-5932.firebaseapp.com',
  databaseURL: 'https://my-linked-in-5932.firebaseio.com',
  projectId: 'my-linked-in-5932',
  storageBucket: 'my-linked-in-5932.appspot.com',
  messagingSenderId: '45167197025',
  appId: '1:45167197025:web:75692fb89f427c74736320',
  measurementId: 'G-KWMQV77LCP'
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MainComponent,
    FooterComponent,
    UpdateProjectComponent,
    ListProjectsComponent,
    ProjectDetailsComponent,
    DeleteProjectModalComponent,
    OtherProjectsComponent,
    PersonalProjectsComponent,
    ListAcademicStudiesComponent,
    ListCertificationsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgbModule,
    NgbButtonsModule,
    ReactiveFormsModule,
    SharedModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    ToastrModule.forRoot(),
    RecaptchaModule,
    RecaptchaFormsModule
  ],
  providers: [
    {provide: RECAPTCHA_SETTINGS, useValue: {siteKey: '6Lem6v4UAAAAAMHbV72_k78EfkDpZbx2bsTERYPR'} as RecaptchaSettings}
  ],
  entryComponents: [DeleteProjectModalComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
