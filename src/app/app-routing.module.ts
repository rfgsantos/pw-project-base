import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainComponent} from './layout/main/main.component';
import {NavbarComponent} from './layout/navbar/navbar.component';
import {UpdateProjectComponent} from './project/update-project/update-project.component';
import {ListProjectsComponent} from './project/list-projects/list-projects.component';
import {ProjectDetailsComponent} from './project/project-details/project-details.component';
import {ProjectResolver} from './shared/resolvers/project.resolver';
import {PersonalProjectsComponent} from './project/personal-projects.component';
import {OtherProjectsComponent} from './project/other-projects.component';
import {ListAcademicStudiesComponent} from './education/academic-studies/list-academic-studies/list-academic-studies.component';
import {ListCertificationsComponent} from './education/certifications/list-certifications/list-certifications.component';

const routes: Routes = [
  {path: '', component: MainComponent},
  {path: 'update-project', component: UpdateProjectComponent},
  {path: 'update-project/:id', component: UpdateProjectComponent, resolve: {project: ProjectResolver}},
  {path: 'list-projects', component: ListProjectsComponent},
  {path: 'personal-projects', component: PersonalProjectsComponent},
  {path: 'other-projects', component: OtherProjectsComponent},
  {path: 'project-details/:id', component: ProjectDetailsComponent, resolve: {project: ProjectResolver}},
  {path: 'list-academic-studies', component: ListAcademicStudiesComponent},
  {path: 'list-certifications', component: ListCertificationsComponent},
  {path: '', outlet: 'navbar', component: NavbarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
